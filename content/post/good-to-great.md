+++
date = "2016-08-04T14:26:17-04:00"
draft = false
title = "good to great"

+++

I read **Good to Great in January 2016**. An awesome read sharing 
detailed analysis on how good companies became great.
